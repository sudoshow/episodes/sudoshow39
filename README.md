# Sudo Show 39: Datacenter Tools
[Episode 39 Playback](https://sudo.show/39)

## Show Notes
Eric and Brandon talk about RHEL 8.5 and RHEL 9 Beta and talk about some interesting tools for managing your datacenter and IT assets.

### Websites
- [Destination Linux Network](https://destinationlinux.network)
- [Sudo Show Website](https://sudo.show)

### Support the Show
- [Sponsor: Bitwarden](https://bitwarden.com/dln)
- [Sponsor: Digital Ocean](https://do.co/dln)
- [Sudo Show Swag](https://sudo.show/swag)

### Contact Us:
- [DLN Discourse](https://sudo.show/discuss)
- [Email Us!](mailto:contact@sudo.show)
- [Sudo Matrix Room](https://sudo.show/matrix)
- [Sudo Show GitLab](https://gitlab.com/sudoshow)

### Follow our Hosts:

- [Brandon's Website](https://open-tech.net)
- [Eric's Website](https://itguyeric.com)
- [Red Hat Streaming](https://www.redhat.com/en/livestreaming)

### Project Links

Asset Management:
https://snipeitapp.com/

CMDB (Content Management Database):
https://www.cmdbuild.org

OS Ticket:
https://osticket.org

IPAM/DCIM:
https://github.com/netbox-community/netbox

### Chapters:
- 00:00 Intro
- 00:42 Welcome
- 02:00 Sponsor: Bitwarden
- 03:02 Sponsor: Digital Ocean
- 04:10 RHEL 8.5 and 9 Beta
- 21:18 Snipit Asset Management
- 33:56 CMDB Build
- 36:11 OSTicket
- 37:44 Netbox
- 46:55 Call to Action
- 48:20 Wrap Up
